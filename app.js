const axios = require('axios');
var regionPair = {};

function getRegionCodesAndTexts() {
  return axios.get('http://api.scb.se/OV0104/v1/doris/sv/ssd/START/ME/ME0104/ME0104D/ME0104T4');
};

function getVoterData() {
  return axios.post('http://api.scb.se/OV0104/v1/doris/sv/ssd/START/ME/ME0104/ME0104D/ME0104T4', {
    "query": [
      {
        "code": "ContentsCode",
        "selection": {
          "filter": "item",
          "values": ["ME0104B8"]
        }
      },
      {
        "code": "Region",
        "selection": {
          "filter": "all",
          "values": ["*"]
        }
      }
    ],
    "response": {
      "format": "json"
    }
  });
};

function pairRegionCodeWithText(response) {
  return new Promise((resolve, reject) => {
    let values = response.data.variables[0].values;
    let valueTexts = response.data.variables[0].valueTexts;
    let numOfValues = Object.keys(response.data.variables[0].values).length;
    let numOfValueTexts = Object.keys(response.data.variables[0].valueTexts).length;

    //Skapar objekt för mappning av regionkod och regiontext
    if (numOfValues === numOfValueTexts) {
      for (var item in values) {
        regionPair[values[item]] = valueTexts[item];
      }
      resolve();
    } else {
      reject('ERROR: Numbers of elements in values and valueTexts does not match. Aborting');
    }   
  });
};

function sortData(response) {
  var responseSubStr = response.data.substring(1); //Skalar bort första whitespace
  var json = JSON.parse(responseSubStr);
  var jsonData = json['data'];
  var sortingObj = {};

  for (var obj in jsonData) {
    let regionCode = jsonData[obj].key[0];
    let year = jsonData[obj].key[1];
    let percentValue = parseFloat(jsonData[obj].values).toFixed(1); //Exemplet hade inte en decimal på jämna % men jag tyckte det såg mer enhetligt ut. Där av denna.
    let regionText = regionPair[regionCode];

    //Lägger till första regionen i sortingObj, exkluderar "Riket"
    if (sortingObj[year] === undefined) {
      if (regionCode !== "00") {
        sortingObj[year] = {year, regionText, percentValue};
      }
      //Vid större procenttal för årtalet uppdateras objektet
    } else if (sortingObj[year].percentValue < percentValue && percentValue !== "NaN") {
      sortingObj[year] = {year, regionText, percentValue};
      //Vid lika procenttal läggs ytterligare en region till i objektet för det årtalet
    } else if (sortingObj[year].percentValue === percentValue && percentValue !== "NaN") {
      regionText = sortingObj[year].regionText + ', ' + regionText;
      sortingObj[year] = {year, regionText, percentValue};
    }
  };
  for (var item in sortingObj) {
    console.log(sortingObj[item].year + ' ' + sortingObj[item].regionText + ' ' + sortingObj[item].percentValue + '%');
  };
};


function runApp() {
  getRegionCodesAndTexts().then((response) => {
    pairRegionCodeWithText(response).then(() => {
      getVoterData().then((response) => {
        sortData(response);
      }).catch((err) => {
        console.log(err);
      })
    }).catch((err) => {
      console.log(err);
    });
  }).catch((err) => {
    console.log((err));
  });
};

runApp();
